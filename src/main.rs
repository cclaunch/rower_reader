extern crate btleplug;
extern crate ctrlc;
extern crate rand;

use btleplug::api::{Central, Peripheral, UUID, ValueNotification};
#[cfg(target_os = "linux")]
use btleplug::bluez::manager::Manager;
#[cfg(target_os = "macos")]
use btleplug::corebluetooth::manager::Manager;
#[cfg(target_os = "windows")]
use btleplug::winrtble::manager::Manager;
use std::thread;
use std::time::Duration;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use bitreader::BitReader;

// https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.heart_rate_measurement.xml
fn hr_value_callback(notification: ValueNotification) {
    println!("{:?}", notification.value);
    let raw: &[_] = &notification.value;
    let mut reader = BitReader::new(raw);
    let rr_interval = reader.read_u8(1).unwrap();
    let energy_expended_status = reader.read_u8(1).unwrap();
    let sensor_contact_status = reader.read_u8(2).unwrap();
    let hr_value_format = reader.read_u8(1).unwrap();
    
    match hr_value_format {
        0 => {
            println!("8-bit hr value format");
            let hr_value = notification.value[1];
            println!("Heart rate = {}", hr_value);
        },
        1 => println!("16-bit hr value format"),
        _ => println!("invalid heat rate format value")
    }

    match sensor_contact_status {
        0 | 1 => println!("Sensor contact feature not supported"),
        2 => println!("Sensor contact supported, but no contacted detected"),
        3 => println!("Sensor contact detected"),
        _ => println!("invalid sensor contact value")
    }

    match energy_expended_status {
        0 => println!("Energy expended field not present"),
        1 => println!("Energy expended field present. Units: kilo Joules"),
        _ => println!("invalid energy expended field status")
    }

    match rr_interval {
        0 => println!("RR-Interval values not present"),
        1 => println!("One or more RR-Interval values present"),
        _ => println!("invalid rr interval status")
    }


}

pub fn main() {
    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();

    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    }).expect("Error setting Ctrl-C handler");

    let manager = Manager::new().unwrap();

    // get the first bluetooth adapter
    let central = manager
        .adapters()
        .expect("Unable to fetch adapter list.")
        .into_iter()
        .nth(0)
        .expect("Unable to find adapters.");

    // start scanning for devices
    central.start_scan().unwrap();
    // instead of waiting, you can use central.event_receiver() to get a channel
    // to listen for notifications on.
    thread::sleep(Duration::from_secs(2));

    // find the device we're interested in
    let hrm = central
        .peripherals()
        .into_iter()
        .find(|p| {
            p.properties()
                .local_name
                .iter()
                .any(|name| name.contains("3E461D2F"))
        })
        .expect("Polar H10 3E461D2F not found.");

    // connect to the device
    hrm.connect().unwrap();

    // discover characteristics
    hrm.discover_characteristics().unwrap();

    // find the characteristic we want
    let chars = hrm.characteristics();

    let hr_measurement = chars
        .iter()
        .find(|c| c.uuid == UUID::B128([0xFB, 0x34, 0x9B, 0x5F, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0x37, 0x2A, 0x00, 0x00]))
        .expect("Unable to find heart rate characteristic.");

    hrm.on_notification(Box::new(hr_value_callback));

    hrm.subscribe(hr_measurement).unwrap();

    println!("Running...");
    while running.load(Ordering::SeqCst) {}
    println!("Exiting on Ctrl-C.");
    hrm.disconnect().unwrap();
}
